@extends('layouts.plantilla')

@section('title' , 'Editar')

@section('content')
    <h1 class="card-title text-center my-3">Edite su curso</h1>
    <div class="row justify-content-center">
        <div class="col-6">
            <form action=" {{route('cursos.update' , $curso)}}" method="POST">
                @csrf
                @method('put')
                <div class="mb-3">
                    <label for="name" class="form-label w-100 text-center">
                        Nombre:
                        <input value="{{$curso->name}}" type="text" name="name" class="form-control" id="name" aria-describedby="name">
                    </label>
                </div>

                <div class="mb-3">
                    <label for="descripcion" class="form-label w-100 text-center">
                        Descripcion:
                        <textarea name="descripcion" id="descripcion" rows="3" class="form-control" id="descripcion"> {{$curso->descripcion}} </textarea>
                    </label>
                </div>

                <div class="mb-3">
                    <label for="categoria" class="form-label w-100 text-center">
                        Categoria:
                        <input value="{{$curso->categoria}}" type="text" name="categoria" class="form-control" id="categoria" aria-describedby="categoria">
                    </label>
                </div>
                <div class="text-center">
                    <button type="submit" class=" btn btn-primary w-50">Guardar</button>

                </div>
            </form>
        </div>
    </div>
@endsection
