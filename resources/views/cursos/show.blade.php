@extends('layouts.plantilla')

@section('title' , $curso->name)

@section('content')

    <a href=" {{route('cursos.index')}} " class="btn btn-primary mt-4 p-2">
        <span class="material-icons fs-4 px-2 align-top">arrow_back_ios</span>
        Volver a los cursos
    </a>


    <div class="row mt-4 border-end border-bottom">
        <div class="col-12">
            <div class="mb-3">
                <div class="row g-0 ">
                    <div class="col-md-5">
                        <img src="https://www.santander.com/content/dam/santander-com/es/stories/cabecera/2020/EfectoSantander/im-storie-santander-lanza-el-plan-be-tech-with-santander-para-contratar-a-500-profesionales-con-perfiles-digitales-desktop.jpg" class="img-fluid" alt="...load">
                    </div>
                    <div class="col-md-7">
                        <div class="card-body border-bottom-0">

                            <div class="d-flex w-100 justify-content-end">
                                <a href=" {{route('cursos.edit' , $curso)}} " class="btn btn-outline-primary a-card a-editar">
                                    <span class="material-icons">build</span>
                                </a>
                                <a href="#" class="btn btn-outline-danger a-card a-eliminar">
                                    <span class="material-icons">delete_outline</span>
                                </a>
                            </div>

                            <div class="">
                                <small class="fw-light text-muted">Autor: <a href="#" class="fw-light text-decoration-none a-autor text-muted fw-bold">Lucas Greco</a></small>
                                
                                <small class="fw-light text-muted"> | </small>

                                <small class="fw-light text-muted">Categoria: </small>
                                <small class="fw-bold text-muted">{{$curso->categoria}} </small>
                            </div>
                            
                            <h1 class="card-title mb-2">{{$curso->name}}</h1>
                            
                            <small class="fw-light text-muted">Descripcion:</small>
                            <p class="card-text px-2">{{$curso->descripcion}}</p>
                            
                            <small class="fw-light text-muted">Subido el 12/34/88</small>
                            
                            

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
