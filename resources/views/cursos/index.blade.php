@extends('layouts.plantilla')

@section('title' , 'Home')

@section('content')

<h1 class="mt-2">Aqui veras todos los cursos</h1>

<a href=" {{route('cursos.create')}} " class="btn btn-info">Crear curso</a>
    
 
     
        <div class="row row-cols-1 row-cols-md-3 g-4 justify-content-center">
            @foreach ($cursos as $curso)
                <div class="col">
                    <div class="card p-2">

                        <img src="https://www.santander.com/content/dam/santander-com/es/stories/cabecera/2020/EfectoSantander/im-storie-santander-lanza-el-plan-be-tech-with-santander-para-contratar-a-500-profesionales-con-perfiles-digitales-desktop.jpg" class="card-img-top img-fluid img-curso p-3 border-l-r border-top" alt="...">
                        <div class="card-body card-body-index border-top-0 border-l-r">
                            <h5 class="card-title">{{$curso->name}}</h5>
                            <p class="card-text">{{$curso->descripcion}}</p>
                        </div>

                        <ul class="list-group list-group-flush">
                            <li class="list-group-item item-creador">
                                <p class="card-text text-white">Lucas Greco</p>
                            </li>
                            <li class="list-group-item item-categoria">
                                <p class="card-text text-white">{{$curso->categoria}}</p>
                            </li>
                            <li class="list-group-item item-fecha">
                                <p class="card-text text-white">21/05/21</p>
                            </li>
                        </ul>

                        <div class="d-flex align-items-end mt-1 d-flex align-items-center">
                            <a href=" {{route('cursos.show' , $curso)}} " class="btn btn-primary a-card w-75 m-1">
                                Ver curso
                            </a>
                            <a href=" {{route('cursos.edit' , $curso)}} " class="btn btn-outline-primary a-card a-editar m-1">
                                <span class="material-icons">build</span>
                            </a>
                            <a href="#" class="btn btn-outline-danger a-card a-eliminar m-1">
                                <span class="material-icons">delete_outline</span>
                            </a>
                        </div>

                    </div>
                </div>
            @endforeach
        </div>

        <div class="text-center">
            <div class="row justify-content-center">
                <div class="col">
                    {{$cursos->links()}}

                </div>
            </div>
        </div>

        
   

@endsection
