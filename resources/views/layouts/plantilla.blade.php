<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags --> 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!--FONT e ICONS-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300;400;700&display=swap" rel="stylesheet">
    <style type="text/css">
    *{
      margin: 0;
      padding: 0;
    }
      body{
        font-family: 'Roboto Condensed', sans-serif;
      }
      .card{
        border: 0px  solid rgb(18, 18, 18) !important;
      }
      .list-group{
        box-shadow: 0px -5px 10px rgb(255, 255, 255) ;
      }
  
      .card-title{
        color: #F05454 !important;
        font-weight: 700;
      }
      .item-creador{
        background-color: #F05454 !important;
      }
      .item-categoria{
        background-color: #30475E !important;
      }
      .item-fecha{
        background-color: #121212 !important;
      }
      .a-card{
        height: auto;
        border: 0px solid !important;
      }  
      .card-body-index {
        height: 160px;
      }
      .border-l-r{
        border-right: 1px solid #42424244 !important;
        border-left: 1px solid #42424244 !important;
      }
      .a-editar{
        color: #30475E;
      }
      .a-autor:hover{
        color: #F05454 !important;
      }
      .a-editar:hover{
        background-color: #30475E
      }
      .material-icons{
        font-size: 1.3em
      }
    </style>

    <div class="container">
      @yield('content')

    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

  </body>
</html>