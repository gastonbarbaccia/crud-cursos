<?php

namespace App\Http\Controllers;

use App\Models\Curso;
use Illuminate\Http\Request;

class cursoController extends Controller
{
    public function index(){
        $cursos = Curso::orderBy('id','desc')->paginate(12);

        return view('cursos.index', compact('cursos'));
    }

    public function create(){
        return view('cursos.create');
    }

    public function store(Request $request){
        $curso = new Curso();

        $curso->name = $request->name;
        $curso->descripcion = $request->descripcion;
        $curso->categoria = $request->categoria;

        $curso->save();

        return redirect()->route('cursos.show' , $curso);
    }

    public function show($id){
        $curso = Curso::find($id); //Devuelve el registro completo con ese ID

        return view('cursos.show' , compact('curso'));
    }

    public function edit(Curso $curso){
        return view('cursos.edit' , compact('curso'));
    }

    public function update(Request $request , Curso $curso){
        $curso->name = $request->name;
        $curso->descripcion = $request->descripcion;
        $curso->categoria = $request->categoria;

        $curso->save();

        return redirect()->route('cursos.show' , $curso);
    }
}
